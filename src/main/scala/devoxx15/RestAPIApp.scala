package devoxx15

import unfiltered.request._
import unfiltered.response._

/**
 * Testing json:
 * - curl http://localhost:8080/json
 * - curl -H "Accept: application/json" -X POST -d '{"name":"test","age":10}' http://localhost:8080/json
 */
object RestAPI {

  case class Person(name: String, age: Int)

  import org.json4s.JsonDSL._
  import org.json4s.native.Serialization
  implicit val formats = org.json4s.DefaultFormats

  val echo = unfiltered.netty.cycle.Planify {
    case GET(Path("/json")) & Params(params) =>
      Ok ~> Json(("conference" -> "Devoxx 2015") ~ ("session" -> "Fast Rest API building with Unfiltered"))
    case req @ POST(Path("/json")) & Accepts.Json(_) =>
      JsonBody(req).map(_.extract[Person]) match {
        case Some(person) =>
          Created ~> JsonContent ~> ResponseString(Serialization.write(person))
        case None =>
          BadRequest ~> ResponseString("Invalid JSON")
      }
  }
}

object RestApiApp extends App {

  unfiltered.netty.Server.anylocal.http(port = 8080)
    .plan(RestAPI.echo)
    .run()
}