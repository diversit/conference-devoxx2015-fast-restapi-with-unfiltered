package devoxx15

import org.scalatest.{ShouldMatchers, WordSpec}
import unfiltered.netty

class RestAPIAppTest extends WordSpec with ShouldMatchers
      with unfiltered.scalatest.netty.Served
      with unfiltered.scalatest.Hosted {

  override def setup: (netty.Server) => netty.Server = {
    _.plan(RestAPI.echo)
  }


  "RestAPI" when {

    "GET /json" should {
      "return json" in {
        val (status, body) = xhttp(host / "json" as_str {
          case (code, response, Some(body), bodyAsString) => (code, bodyAsString())
        })

        status should be (200)
        body should be ("""{"conference":"Devoxx 2015","session":"Fast Rest API building with Unfiltered"}""")
      }
    }

    "POST /json" should {
      "return json" in {
        val applicationJson = "application/json"
        val acceptJsonHeader = Map("Accept" -> applicationJson)
        val jsonBody = """{"name":"Joost","age":42}"""

        val (status, body) = xhttp(host / "json" <:< acceptJsonHeader << (jsonBody, applicationJson) as_str {
          case (code, response, Some(body), bodyAsString) => (code, bodyAsString())
        })

        status should be (201)
        body should be (jsonBody)
      }
    }
  }

}
