name := "Devoxx Unfiltered"

version := "1.0"

scalaVersion := "2.11.6"

organization := "DiversIT Europe"

Revolver.settings

libraryDependencies ++= {
  val unfilteredVersion = "0.8.4"
  val akkaVersion = "2.3.11"
  Seq(
    "net.databinder" %% "unfiltered-netty-server" % unfilteredVersion,
    "net.databinder" %% "unfiltered-directives" % unfilteredVersion,
    "net.databinder" %% "unfiltered-json4s" % unfilteredVersion,
    "net.databinder" %% "unfiltered-netty-websockets" % unfilteredVersion,
    "net.databinder" %% "unfiltered-scalatest" % unfilteredVersion % Test
//    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
//    "com.typesafe.akka" %% "akka-persistence-experimental" % akkaVersion,
//    "com.typesafe.akka" %% "akka-contrib" % akkaVersion,
//    "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.1.6" % Test
  )
}

mainClass in Revolver.reStart := Some("devoxx15.RestAPIApp")
