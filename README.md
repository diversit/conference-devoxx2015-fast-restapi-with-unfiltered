# Fast Rest API building with Unfiltered #

This repository contains the KeyNote presentation for [my session on **Devoxx 2015**](http://cfp.devoxx.be/2015/talk/CCF-5732/Fast_Rest_API_building_with_Unfiltered) about building a Rest API with the Unfiltered framework.

The repository also contains all sources used in the presentation.

### Unfiltered resources ###

* Unfiltered site : http://unfiltered.databinder.net/

* Unfiltered Github : https://github.com/unfiltered (contains many g8 templates to get started with Unfiltered)

* Unfiltered example app using WebSockets and Akka (Clustering): https://bitbucket.org/diversit/dive-into-scala-class
This example was created for a Scala class at the NextBuild 2015 conference.

#### ScalaDoc #####

The ScalaDoc for Unfiltered is not always easy to find.
Here are some resource links. Others can be found on the [javadoc.io](http://www.javadoc.io) site.

* Unfiltered : http://www.javadoc.io/doc/net.databinder/unfiltered_2.11/0.8.4
* Unfiltered Netty : http://www.javadoc.io/doc/net.databinder/unfiltered-netty_2.11/0.8.4
* Unfiltered Netty Server : http://www.javadoc.io/doc/net.databinder/unfiltered-netty-server_2.11/0.9.0-beta1
* Dispatch : dispatch-classic.databinder.net (can be used for testing or async http client)